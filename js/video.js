/*
 * SPDX-FileCopyrightText: 2019 (c) Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * ECMAScript module for converting video in webm format to peertube iframe
 * Usage: ```
 * <video class="img-fluid" loop muted autoplay data-peertube="https://peertube.mastodon.host/videos/embed/ae19d1ec-dee7-41b2-a9b7-d78d0ea63411">
 *  <source src="/announcements/announce-applications-1908/konsole-tabs.webm" type="video/webm">
 * </video>```
 */
export default class Video {
    constructor() {
        const video = document.createElement('video');
        this.supportWebm = video.canPlayType('video/webm; codecs="vp8, vorbis"') === "probably";
    }

    convertPeertube() {
        if (!this.supportWebm) {
            // safari :(
            // change link to peertube
            const width = document.querySelector('.releaseAnnouncment').clientWidth;
            const height = 315 / 560 * width;

            // get all videos
            document.querySelectorAll('video').forEach(video => {
                // replace video element with peertube iframe
                peertube_link = video.dataset.peertube;
                const iframe = document.createElement('iframe');
                iframe.width = width;
                iframe.height = height;
                iframe.sandbox = "allow-same-origin allow-scripts";
                iframe.src = peertube_link;
                iframe.frameborder = 0;
                iframe.allowfullscreen = true;
                video.replaceWith(iframe);
            });

            this.resizeVideo();
        }
    }

    /**
     * Resize all videos to parent size
     */
    resizeVideo() {
        // Resize iframe to .releaseAnnouncment width
        window.addEventListener('resize', function(event) {
            document.querySelectorAll('iframe').forEach(iframe => {
                const width = iframe.parent.clientWidth;
                const height = 315 / 560 * width;
                iframe.width = width;
                iframe.height = height;
            });
        });
    }
}
